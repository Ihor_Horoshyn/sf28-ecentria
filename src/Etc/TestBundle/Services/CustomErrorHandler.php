<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Etc\TestBundle\Services;

/**
 * Description of CustomErrorHandler
 *
 * @author sc
 */
class CustomErrorHandler {
    //put your code here
    public function getLastError(){
        $errorInfo = error_get_last();
        $errorString = " Error type {$errorInfo['type']}, {$errorInfo['message']} on line {$errorInfo['line']} of " .
            "{$errorInfo['file']}. ";
        return $errorString;
    }
}
