<?php
namespace Etc\TestBundle\Services;

use Doctrine\ORM\EntityManager;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomEntityManager
 *
 * @author sc
 */
class CustomEntityManager extends EntityManager{
    
    protected $_entities = array();
        
    protected $_entityIdToPrimary = array();

    protected $_entityPrimaryToId = array();

    protected $_entitySaveList = array();

    protected $_nextId = null;

    protected $_dataStore = null;
    
    public function __construct($serviceContainer,$storePath) {
        $em = $serviceContainer->get('doctrine.orm.default_entity_manager');
        $connection = $em->getConnection();
        $config = $em->getConfiguration();
        $eventManager = $em->getEventManager();
        parent::__construct($connection, $config, $eventManager);
        if($storePath){
            $this->_dataStore = new DataStore($storePath);
            
            $this->_nextId = 1;
            
            $itemTypes = $this->_dataStore->getItemTypes();
            foreach ($itemTypes as $itemType)
            {
                $itemKeys = $this->_dataStore->getItemKeys();
                foreach ($itemKeys as $itemKey) {
                    $entity = $this->create($itemType, $this->_dataStore->get($itemType, $itemKey), true);
                }        
            }
        }
    }
    
    
    //create an entity
    public function create($entityName, $data, $fromStore = false)
    {
        $entity = new $entityName;
        $entity->_entityName = $entityName;
        $entity->_data = $data;
        $entity->_em = Entity::getDefaultEntityManager();
        $id = $entity->_id = $this->_nextId++;
        $this->_entities[$id] = $entity;
        $primary = $data[$entity->getPrimary()];
        $this->_entityIdToPrimary[$id] = $primary;
        $this->_entityPrimaryToId[$primary] = $id;
        if ($fromStore !== true) {
            $this->_entitySaveList[] = $id;
        }

        return $entity;
    }

    //update
    public function update($entity, $newData)
    {
        if ($newData === $entity->_data) {
            //Nothing to do
            return $entity;
        }

        $this->_entitySaveList[] = $entity->_id;
        $oldPrimary = $entity->{$entity->getPrimary()};
        $newPrimary = $newData[$entity->getPrimary()];
        if ($oldPrimary != $newPrimary)
        {
            $this->_dataStore->delete(get_class($entity),$oldPrimary);
            unset($this->_entityPrimaryToId[$oldPrimary]);
            $this->_entityIdToPrimary[$entity->$id] = $newPrimary;
            $this->_entityPrimaryToId[$newPrimary] = $entity->$id;    
        }
        $entity->_data = $newData;

        return $entity;
    }

    //Delete
    public function delete($entity)
    {
        $id = $entity->_id;
        $entity->_id = null;
        $entity->_data = null;
        $entity->_em = null;
        $this->_entities[$id] = null;
        $primary = $entity->{$entity->getPrimary()};
        $this->_dataStore->delete(get_class($entity),$primary);
        unset($this->_entityIdToPrimary[$id]);
        unset($this->_entityPrimaryToId[$primary]);
        return null;
    }

    public function findByPrimary($entity, $primary)
    {
        if (isset($this->_entityPrimaryToId[$primary])) {
            $id = $this->_entityPrimaryToId[$primary];
            return $this->_entities[$id];
        } else {
            return null;
        }
    }

    //Update the datastore to update itself and save.
    public function updateStore() {
        foreach($this->_entitySaveList as $id) {
            $entity = $this->_entities[$id];
            $this->_dataStore->set(get_class($entity),$entity->{$entity->getPrimary()},$entity->_data);
        }
        $this->_dataStore->save();
    }
    
}
