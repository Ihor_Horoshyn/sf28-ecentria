<?php

namespace Etc\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InventoryItem
 *
 * @ORM\Table(name="inventory_item")
 * @ORM\Entity(repositoryClass="Etc\TestBundle\Repository\InventoryItemRepository")
 */
class InventoryItem extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="_data", type="string", length=255)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="qoh", type="string", length=255)
     */
    private $qoh;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return InventoryItem
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set qoh
     *
     * @param string $qoh
     *
     * @return InventoryItem
     */
    public function setQoh($qoh)
    {
        $this->qoh = $qoh;

        return $this;
    }

    /**
     * Get qoh
     *
     * @return string
     */
    public function getQoh()
    {
        return $this->qoh;
    }
    
    
    //Update the number of items, because we have shipped some.
    public function itemsHaveShipped($numberShipped)
    {
        $current = $this->qoh;
        $current -= $numberShipped;
        $newData = $this->_data;
        $newData['qoh'] = $current;
        $this->update($newData);

    }

    //We received new items, update the count.
    public function itemsReceived($numberReceived)
    {

        $newData = $this->_data;
        $current = $this->qoh;

        for($i = 1; $i <= $numberReceived; $i++) {
            //notifyWareHouse();  //Not implemented yet.
            $newData['qoh'] = $current++;
        }
        $this->update($newData);
    }

    public function changeSalePrice($salePrice)
    {
            $newData = $this->_data;
            $newData['salePrice'] = $this->update($newData);
    }

    public function getMembers()
    {
        //These are the field in the underlying data array
        return array("sku" => 1, "qoh" => 1, "cost" => 1, "salePrice" => 1)    ;
    }

    public function getPrimary()
    {
        //Which field constitutes the primary key in the storage class?
        return "sku";
    }
}

