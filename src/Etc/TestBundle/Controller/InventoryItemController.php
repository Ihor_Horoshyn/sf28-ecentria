<?php

namespace Etc\TestBundle\Controller;

use Etc\TestBundle\Entity\InventoryItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Inventoryitem controller.
 *
 * @Route("inventoryitem")
 */
class InventoryItemController extends Controller
{
    /**
     * Lists all inventoryItem entities.
     *
     * @Route("/", name="inventoryitem_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        
        $inventoryItems = $em->getRepository('Etc\TestBundle\Entity\InventoryItem')->findAll();

        return $this->render('inventoryitem/index.html.twig', array(
            'inventoryItems' => $inventoryItems,
        ));
    }

    /**
     * Creates a new inventoryItem entity.
     *
     * @Route("/new", name="inventoryitem_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $inventoryItem = new Inventoryitem();
        $form = $this->createForm('Etc\TestBundle\Form\InventoryItemType', $inventoryItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inventoryItem);
            $em->flush();

            return $this->redirectToRoute('inventoryitem_show', array('id' => $inventoryItem->getId()));
        }

        return $this->render('inventoryitem/new.html.twig', array(
            'inventoryItem' => $inventoryItem,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inventoryItem entity.
     *
     * @Route("/{id}", name="inventoryitem_show")
     * @Method("GET")
     */
    public function showAction(InventoryItem $inventoryItem)
    {
        $deleteForm = $this->createDeleteForm($inventoryItem);

        return $this->render('inventoryitem/show.html.twig', array(
            'inventoryItem' => $inventoryItem,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inventoryItem entity.
     *
     * @Route("/{id}/edit", name="inventoryitem_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, InventoryItem $inventoryItem)
    {
        $deleteForm = $this->createDeleteForm($inventoryItem);
        $editForm = $this->createForm('Etc\TestBundle\Form\InventoryItemType', $inventoryItem);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inventoryitem_edit', array('id' => $inventoryItem->getId()));
        }

        return $this->render('inventoryitem/edit.html.twig', array(
            'inventoryItem' => $inventoryItem,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inventoryItem entity.
     *
     * @Route("/{id}", name="inventoryitem_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, InventoryItem $inventoryItem)
    {
        $form = $this->createDeleteForm($inventoryItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inventoryItem);
            $em->flush();
        }

        return $this->redirectToRoute('inventoryitem_index');
    }

    /**
     * Creates a form to delete a inventoryItem entity.
     *
     * @param InventoryItem $inventoryItem The inventoryItem entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InventoryItem $inventoryItem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inventoryitem_delete', array('id' => $inventoryItem->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
