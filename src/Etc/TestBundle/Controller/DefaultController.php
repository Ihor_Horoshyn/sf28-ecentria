<?php

namespace Etc\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Etc\TestBundle\Entity\BaseEntity as MyBaseEntity;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response(
            '<html><body>Lucky number: ' . $number . '</body></html>'
        );
    }
}
